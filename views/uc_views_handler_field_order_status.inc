<?php

/**
 * @file
 * Views handler: Order status field.
 */

/**
 * Return a formatted weight value to display in the View.
 */
class uc_views_handler_field_order_status extends views_handler_field {
  var $status_options = array();

  /**
   * Construct a new field handler.
   */
  function construct() {
    parent::construct();
    $options = uc_order_status_list();
    foreach (uc_order_status_list() as $status) {
      $this->status_options[$status['id']] = $status['title'];
    }
  }

  function render($values) {
    $val = $values->{$this->field_alias};
    return isset($this->status_options[$val]) ? $this->status_options[$val] : $val;
  }
}
